#! /bin/sh

set -e
chmod +x /usr/local/bin/dropbox_uploader.sh

if [ "${SCHEDULE}" = "" ]; then
  sh backup.sh
else
  exec go-cron "$SCHEDULE" /bin/sh backup.sh
fi
