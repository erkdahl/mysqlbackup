#!/bin/sh

set -eu

MYSQL_HOST_OPTS="-h $MYSQL_HOST -P $MYSQL_PORT -u$MYSQL_USER -p$MYSQL_PASSWORD"
databases=$(mysql $MYSQL_HOST_OPTS -e 'SHOW DATABASES;' --silent)

for database in $databases
do
    if [ "$database" != "information_schema" ] && [ "$database" != "sys" ] && [ "$database" != "mysql" ] && [ "$database" != "performance_schema" ]
    then
        echo "Creating backup for $database..."
        mysqldump $MYSQL_HOST_OPTS $MYSQLDUMP_OPTIONS $database | gzip > $database.sql.gz
        /usr/local/bin/dropbox_uploader.sh upload $database.sql.gz $database-$(date +%Y-%m-%d).sql.gz
        rm -fr $database.sql.gz
    fi
done
